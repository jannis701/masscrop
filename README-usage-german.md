### MassCrop Benutzung

`masscrop.py -i <inputdir/inputfile> [-o <outputdir> -x <maxwidth> -y <maxheight>  -c <configfile> -C -a]`

- `-i`: Pfad zur Bilddatei oder zum Verzeichnis mit Bildern, welche bearbeitet werden sollen
- `-o` (optional): Pfad zum Verzeichnis, in dem die Bilder gespeichert werden. Default: Unterordner `output` im Verzeichnis der zu bearbeitenden Bilder
- `-x`, `-y` (optional): maximal-Breite (`-x`) und -Höhe (`-y`) für die Anzeige der Biilder. Default: beides auf Bildschirmhöhe minus 100 Pixel
- `-a` (optional): bereits im Zielverzeichnis vorhandene Bilder nicht überspringen, sondern alle Bilder bearbeiten (vorhandene werden überschrieben)
- `-c` (optional): Pfad zur Konfigurations-Datei. Default: config.json im Ordner des Skripts
- `-C` (optional): Kalibrierung ausführen

Wenn die Konfiguartionsdatei nicht existiert, wird automatisch die Kalibrierung ausgeführt (wie wenn `-C` angegeben wäre)

#### Zuschneiden:

Eingabemöglichkeiten:
- `H`: Bild 1° nach links drehen
- `J`: Bild 1° nach rechts drehen
- `G`: Bild 90° nach links drehen (nur möglich, wenn noch keine 1°-Rotation angewendet wurde)
- `K`: Bild 90° nach rechts drehen (nur möglich, wenn noch keine 1°-Rotation angewendet wurde)
- `E`: Mit der Maus einen eigenen Ausschnitt zeichnen und ggf. vorgeschlagenen Ausschnitt verwerfen. `ENTER` oder `LEERTASTE` zum Bestätigen des Ausschnitts 
- `C`: Bild ignorieren und nicht speichern, nächstes Bild
- `LEERTASTE` oder `ENTER`:  Bild-Ausschnitt speichern (oder wenn kein Ausschnitt vorhanden, dann gesamtes Bild speichern)
- `P` : Kalibrierung ändern (siehe unten)
- `Q` oder `ESC`: Beenden

#### Kalibrieren:
Wenn man "K" gedrückt hat, oder wenn keine Konfigurations-Datei gefunden wurde, kommt man in die Kalibrierung. Diese funktioniert wie folgt:

1. Ein Bild für die Kalibrierung im Datei-Dialog auswählen
2. Die 6 Regler so einstellen, dass das zentrale Objekt selbst deutlich schwarz überdeckt ist und der Hintergrund um das Objekt herum möglichst komplett sichtbar
3. Dabei versuchen, bei den Reglern, die auf "H" enden möglichst niedrige Werte zu finden und bei den Reglern, die auf "L" enden, möglichst hohe Werte, aber immer so, dass die Bedingung aus Schritt 2. noch erfüllt ist.
4. Wenn die Einstellung gut ist `ENTER` oder `LEERTASTE` drücken zum speichern, oder aber `Q` oder `ESC` drücken zum Abbrechen ohne speichern
