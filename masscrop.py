#!/bin/python3

import cv2 as cv
import numpy as np
import os
import sys, getopt
from pathlib import Path
import json
import  tkinter as tk
from tkinter import  filedialog, messagebox

def getResizeFactor(screenW, screenH, w, h):
    if h * (screenW / w) > screenH:
        return screenH / h
    else:
        return screenW / w

def getSuggestion(img, w, h):
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    thresh = cv.inRange(hsv, tuple(configData["threshRange"]["low"]), tuple(configData["threshRange"]["high"]));

    contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    boxes = {}
    for cnt in contours:
        box = cv.boundingRect(cnt)

        #cv.rectangle(thresh,(box[0],box[1]),(box[0]+box[2],box[1]+box[3]),(0,0,255),2)
        #cv.imshow('Bild2',thresh)
        if box[2]*box[3] > w*h/15 and box[1] > h/20 and box[0] > w/20 and box[0]+box[2] < w/20*19 and box[1]+box[3] < h/20*19:
            boxes[int(box[2]*box[3])] = box
    if len(boxes)>0:
        box = boxes[max(boxes)]
        eF = 30
        return tuple([sum(x) for x in zip(box,(int(-1*w/eF),int(-1*h/eF),int(2*w/eF),int(2*h/eF)))])
    return (0,0,0,0)

def getThreshRange(imgConfig): 

    def nothing(x):
        pass

    cv.namedWindow('config')

    # create trackbars for color change
    cv.createTrackbar('lowH','config',configData["threshRange"]["low"][0],179,nothing)
    cv.createTrackbar('highH','config',configData["threshRange"]["high"][0],179,nothing)

    cv.createTrackbar('lowS','config',configData["threshRange"]["low"][1],255,nothing)
    cv.createTrackbar('highS','config',configData["threshRange"]["high"][1],255,nothing)

    cv.createTrackbar('lowV','config',configData["threshRange"]["low"][2],255,nothing)
    cv.createTrackbar('highV','config',configData["threshRange"]["high"][2],255,nothing)

    ret = None
    while True:
        # get current positions of the trackbars
        ilowH = cv.getTrackbarPos('lowH', 'config')
        ihighH = cv.getTrackbarPos('highH', 'config')
        ilowS = cv.getTrackbarPos('lowS', 'config')
        ihighS = cv.getTrackbarPos('highS', 'config')
        ilowV = cv.getTrackbarPos('lowV', 'config')
        ihighV = cv.getTrackbarPos('highV', 'config')

        # convert color to hsv because it is easy to track colors in this color model
        hsv = cv.cvtColor(imgConfig, cv.COLOR_BGR2HSV)
        lower_hsv = np.array([ilowH, ilowS, ilowV])
        higher_hsv = np.array([ihighH, ihighS, ihighV])
        # Apply the cv.inrange method to create a mask
        mask = cv.inRange(hsv, lower_hsv, higher_hsv)
        # Apply the mask on the image to extract the original color
        frame = cv.bitwise_and(imgConfig, imgConfig, mask=mask)
        cv.imshow('config', frame)
        # Press q to exit
        k = cv.waitKey(100) & 0xFF
        if k == ord('q') or k == 27: # 27 = ESC
            break
        elif k == 13 or k == 32: # 13 = ENTER, 32 = leertaste
            ret = {"low": [ilowH, ilowS, ilowV], "high": [ihighH, ihighS, ihighV]}
            break

    cv.destroyAllWindows()

    return ret

def configThreshRange():
    filePath = filedialog.askopenfilename(filetypes=[("Bilder",('*\.[jJ][Pp][Gg]','*\.[jJ][pP][eE][gG]', '*\.[pP][nN][gG]'))])
    if len(filePath) < 1:
        return

    imgConfig = cv.imread(filePath)
    hOrg, wOrg, chOrg = imgConfig.shape
    f = getResizeFactor(screenWidth, screenHeight-300, wOrg, hOrg)
    w, h = int(wOrg * f), int(hOrg * f)
    imgConfig = cv.resize(imgConfig, (w,h))

    newThreshRange = getThreshRange(imgConfig)
    if newThreshRange != None:
        configData["threshRange"] = newThreshRange
        jsonFile = open(configFile, "w")
        json.dump(configData, jsonFile)
        jsonFile.close()
        messagebox.showinfo("OK", "Kalibrierung gespeichert")
    else:
        messagebox.showwarning("Achtung", "Kalibrierung abgebrochen")

root = tk.Tk()
root.withdraw()
screenWidth = 0
screenHeight = 0
quitRequest = False
inputDir = 'input'
outputDir = ''
configFile = os.path.dirname(__file__) + "/config.json"
skipExisting = True
mode = "run"

opts, args = getopt.getopt(sys.argv[1:],"haCi:o:x:y:c:")
for opt, arg in opts:
    if opt == "-i":
        inputDir = arg
    elif opt == "-o":
        outputDir = arg
    elif opt == "-x":
        screenWidth = int(arg)
    elif opt == "-y":
        screenHeight = int(arg)
    elif opt == "-a":
        skipExisting = False
    elif opt == "-c":
        configFile = arg
    elif opt == "-C":
        mode = "config"
    else:
        print ('masscrop.py -i <inputdir/inputfile> [-o <outputdir> -x <maxwidth> -y <maxheight> -c <configfile> -C -a]')
        sys.exit()

if screenWidth == 0 or screenHeight == 0:
    screenWidth = screenHeight = min(root.winfo_screenheight(),root.winfo_screenwidth())-100

jsonFile = open(configFile, "r")
jsonFileContent = jsonFile.read()
jsonFile.close()
if len(jsonFileContent) < 2:
    configData = {"threshRange": {"low": [50,0,100], "high": [100,70,255]}}
    mode = "config"
else:
    configData = json.loads(jsonFileContent)

if mode == "config":
    configThreshRange()

extensions = [".[jJ][pP][eE][gG]",".[jJ][pP][gG]",".[pP][nN][gG]"]
images = []
if not os.path.exists(inputDir):
    messagebox.showerror("Fehler", "Ungültiges Verzeichnis/Datei " + inputDir)
    sys.exit()
if os.path.isdir(inputDir):
    for ext in extensions:
        images.extend(Path(inputDir).glob('./*' + ext))
else:
    p = Path(inputDir)
    if p.suffix.lower() in (".jpg",".jpeg",".png"):
        images.append(p)
    else:
        messagebox.showerror("Fehler", "Enthält kein Bild: " + inputDir)
        sys.exit()

if outputDir == '':
    if os.path.isdir(inputDir):
        outputDir = inputDir + '/output'
    else:
        outputDir = os.path.dirname(os.path.abspath(p.as_posix())) + '/output'
        
if not os.path.exists(outputDir):
    os.mkdir(outputDir)
elif not os.path.isdir(outputDir):
    messagebox.showerror("Fehler", "Ungültiges Zielverzeichnis: " + outputDir)
    sys.exit()

filePaths = sorted([i.as_posix() for i in images])

for imagePath in filePaths:
    imageName = os.path.basename(imagePath)
    outputPath = outputDir + "/" + imageName
    if skipExisting and os.path.exists(outputPath):
        continue

    rotation = 0
    ignoreRequest = False
    imgOrg = cv.imread(imagePath)
    hOrg, wOrg, chOrg = imgOrg.shape
    f = getResizeFactor(screenWidth, screenHeight, wOrg, hOrg)
    w, h = int(wOrg * f), int(hOrg * f)
    img = cv.resize(imgOrg, (w,h))
    print(imageName)
    windowName = "Bild"
    cv.namedWindow(windowName)

    cropBox = getSuggestion(img,w,h)
    imgSuggest = img.copy()
    imgRot = img.copy()
    if cropBox != (0,0,0,0):
        cv.rectangle(imgSuggest, cropBox, (0,0,255), 2)
    cv.imshow(windowName,imgSuggest)

    while(True):
        k = cv.waitKey(-1)
        if k == ord("e"):
            cropBox = cv.selectROI(windowName, imgRot, False)
            break
        elif k == ord("c"):
            ignoreRequest = True
            break
        elif k == ord("q") or k == 27: # 27 = ESC
            quitRequest = True
            break
        elif k == 13 or k == 32: # 13 = ENTER, 32 = leertaste
            break
        elif k == ord("p"):
            configThreshRange()
        elif k == ord("g") or k == ord("k"):
            if rotation != 0:
                print("Warnung: Bild wurde schon um einzelne Grade rotiert")
                continue
            rot90 = cv.ROTATE_90_CLOCKWISE
            if k == ord("g"):
                rot90 = cv.ROTATE_90_COUNTERCLOCKWISE
            imgOrg = cv.rotate(imgOrg, rot90)
            img = cv.rotate(img, rot90)
            imgSuggest = img.copy()
            w, h = h, w
            wOrg, hOrg = hOrg, wOrg
        elif k == ord("h") or k == ord("j"):
            rotation += (ord("i") - k) # i is between "h" and "i"
            (cX, cY) = (w // 2, h // 2)
            M = cv.getRotationMatrix2D((cX, cY), rotation, 1.0)
            imgSuggest = cv.warpAffine(img, M, (w, h))
        imgRot = imgSuggest.copy()
        cropBox = getSuggestion(imgSuggest, w, h)
        if cropBox != (0, 0, 0, 0):
            cv.rectangle(imgSuggest, cropBox, (0, 0, 255), 2)
        cv.imshow(windowName,imgSuggest)

    if ignoreRequest:
        continue

    if quitRequest:
        break

    if rotation != 0:
        (cXorg, cYorg) = (wOrg // 2, hOrg // 2)
        M = cv.getRotationMatrix2D((cXorg, cYorg), rotation, 1.0)
        imgOrg = cv.warpAffine(imgOrg, M, (wOrg, hOrg))

    if cropBox != (0,0,0,0):
        cropBoxScaled = [int(x/f) for x in cropBox]
        imgCrop = imgOrg[cropBoxScaled[1]: cropBoxScaled[1]+cropBoxScaled[3], cropBoxScaled[0]: cropBoxScaled[0]+cropBoxScaled[2]]
        cv.imwrite(outputPath, imgCrop)
    else:
        cv.imwrite(outputPath, imgOrg)

cv.destroyAllWindows()
