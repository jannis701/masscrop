### MassCrop

Get only the central object of an image (and of lots of images). Uses [openCV](https://opencv.org/) to crop an image that only the central object of the image remains.

Features:

- automatically suggest crop-region, if possible
- visually calibrate settings for automatic extraction for a given background
- rotate Image before cropping


### Requirements
- Python 3.x
- Python module tkinter 
- Python module opencv

Debian:

`sudo apt-get install python3 python3-opencv python3-tk`

### Usage

[Deutsche Version](README-usage-german.md)

`masscrop.py -i <inputdir/inputfile> [-o <outputdir> -x <maxwidth> -y <maxheight>  -c <configfile> -C -a]`

- `-i`: path to file or directory to process
- `-o` (optional): path to directory to put output-files to. Default: subfolder `output` in the inputdir or in the directory where the inputfile is
- `-x`, `-y` (optional): max width (`-x`) and max height (`-y`) to display images. Default: set both to screen-height minus 100 pixels
- `-a` (optional): process all images, even if there is already an image in outputdir (which will be overwritten)
- `-c` (optional): path to config-file. Default: config.json in the script's directory
- `-C` (optional): run calibration

If the config-file does not exist, `-C` is forced to run calibration.

#### Crop:
- Use the following Keys
  - `H`: rotate image 1° left
  - `J`: rotate image 1° right
  - `G`: rotate image 90° left (only possible if no 1°-rotation was done before)
  - `K`: rotate image 90° right (only possible if no 1°-rotation was done before)
  - `E`: draw your own section and discard the proposed section, if there was any. ENTER or SPACEBAR to confirm the section
  - `C`: continue to the next image, ignore and therefore do not save the current one
  - `SPACEBAR` or `ENTER`: confirm the section and save the image to output-directory. If no section is drawn, the whole image will be saved.
  - `P` : run calibration
  - `Q` or `ESC`: Quit

#### Calibration:

1. choose an image for the calibration from file-open-dialog
2. Adjust the 6 trackbars to have the object covered with black and the background very well visible.
3. Try to adjust the trackbars in a way that the ones ending with "H" have lowest possible values and the one with "L" have highest possible values, still fulfilling the conditions of step 2.
4. Press `ENTER` or `SPACEBAR` to confirm and save the selection. Press `Q` or `ESC` to cancel the calibration without saving. 

### Integration to filemanagers
#### Nautilus
You can use "scripts" of Nautilus filemanage to run the tool from right-mouse-contextmenu:

- Create a file in the folder `./local/share/nautilus/scripts` with the following content:

```bash
#!/bin/bash

while [ "x$1" != "x" ]; do
    # adjust path to masscrop.py-script
    /bin/python3 /path/to/masscrop.py -i "$1"
    shift
done
```
- Adjust the path to the masscrop-script in the file
- make the file executable with `chmod 755 ./local/share/nautilus/scripts/script-name`


### License and used Software
The software itself is licensed unter the MIT License (see [License.txt](License.txt)).

The following software is used in this project:

- [openCV](https://opencv.org/) 
